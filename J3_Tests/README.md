# Unit tests

En grande partie inspirée du livre ["The Art Of Unit testing with examples in C# (Second Edition)"](https://www.amazon.fr/Art-Unit-Testing-Roy-Osherove/dp/1617290890/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=the+art+of+unit+testing&qid=1574200663&s=english-books&sr=1-1) de Roy OSHEROVE.

## Bons unit tests

* Automatisé et répétable
* Simple à implémenter
* Rapide
* Retourne toujours le même résultat s'il n'y a aucun changement dans le code
* **Isolé**
* Facile à comprendre

Les questions à se poser:

* Est-ce qu'il est possible d'obtenir le résultat des unit-tests écrits il y a 2 mois/ans ?
* Est-ce que n'importe quel membre de mon équipe peut exécuter les unit-tests et obtenir des résultats ?
* Est-ce qu'il est possible d'exécuter l'ensemble des unit-tests et d'en obtenir les résultats en moins de quelques minutes ?
* Est-ce qu'il est possible d'exécuter l'ensemble des unit-tests en appuyant sur un seul bouton ?
* Est-ce qu'il est possible d'écrire un unit-tests en quelques minutes ?

## 3 Types de Unit-tests

* Retour de fonction
* Changement d'état d'un objet
* Appel d'un autre objet

## 1er Unit-Test

1. Choisir et installer son Framework (xUnit, NUnit ou MSTest V2).
2. Créer un projet de test
   1. Créer une classe de test
   2. Créer une méthode de test
3. Les trois étapes d'un Unit-Test
   1. **Arrange**
   2. **Act**
   3. **Assert**

## Flow

```
AssemblyInitialize
    ClassInitialize

        TestInitialize
            # Test1 #
        TestCleanup

        TestInitialize
            # Test2 #
        TestCleanup
        
    ClassCleanup

    ClassInitialize
        ...
    ClassCleanup

AssemblyCleanup

```

[Link to Cheat sheet](https://www.automatetheplanet.com/nunit-cheat-sheet/)

## Organiser ses tests

Projets, dossiers/namespaces, convention de nommage, attributs

## Fakes

Indépendances avec des **Stubs**

Interactions avec des **Mocks** (Constrained et Unconstrained)

## Rappel des 3 piliers les plus important pour du unit-testing

1. Confiance
2. Maintenabilité
3. Lisibilité
